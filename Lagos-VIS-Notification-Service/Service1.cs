﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.IO;
using NLog;
using System.Configuration;
using System.Data.SqlClient;

namespace Lagos_VIS_Notification_Service
{
    public partial class Service1 : ServiceBase
    {
        public Service1()
        {
            InitializeComponent();
        }
        private static Logger logger = LogManager.GetCurrentClassLogger();
        protected override void OnStart(string[] args)
        {
            logger.Info("SERVICE STARTED AT " + DateTime.Now);
            // Set up a timer that triggers every minute.
            int interval = Convert.ToInt32(ConfigurationManager.AppSettings["interval"]);
            Timer timer = new Timer();
            timer.Interval = interval; //60000; // 60 seconds
            timer.Elapsed += new ElapsedEventHandler(this.OnTimer);
            timer.Start();
        }

        protected override void OnStop()
        {
        }


        public void OnTimer(object sender, ElapsedEventArgs args)
        {
            //CHECK FOR THE EXISTENCE OF LOG FOLDER
            try
            {
                if (!Directory.Exists("C:\\LOG-NOTIFICATION-LAGOS-VIS"))
                {
                    Directory.CreateDirectory("C:\\LOG-NOTIFICATION-LAGOS-VIS");
                }
            }
            catch (Exception ex)
            {
                var err = ex.Message;
            }

            SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["XpressDb"].ConnectionString);
            cn.Open();
            try
            {
                
                // string query = " select C.MerchantCode,p.TransId,tbl.Amount,p.MaskedCardPan,p.TerminalId,p.DepositorName,p.CustomerName,p.RevenueCode,p.TransactionDate,p.Currency,p.ReceiptNumber,p.InstitutionCode,p.Bank,p.BankName,p.BankBranch,c.NotificationEndPoint,p.ReceiptNumber,p.Amount from PaymentTransactions p, ConsultantInfo c,tbl_Lagos_VIS tbl where  and isPush IS NULL and cast(p.TransactionDate as Date) = cast(getdate() as Date) and c.ConsultantCode = 'ITC'";
                // string query = "select C.MerchantCode,p.TransId,tbl.Amount,p.MaskedCardPan,p.TerminalId,p.DepositorName,p.CustomerName,p.RevenueCode,p.TransactionDate,p.Currency,p.ReceiptNumber,p.InstitutionCode,p.Bank,p.BankName,p.BankBranch,c.NotificationEndPoint,p.ReceiptNumber,tbl.Amount from PaymentTransactions p, ConsultantInfo c,tbl_Lagos_VIS tbl where P.InstitutionCode='2039LA003626928' and isPush IS NULL and c.ConsultantCode = 'SOFTA' and tbl.revenueCode=p.RevenueCode";
                 string query = "select C.MerchantCode,p.TransId,p.Amount,p.MaskedCardPan,p.TerminalId,p.DepositorName,p.CustomerName,p.RevenueCode,p.TransactionDate,p.Currency,p.ReceiptNumber,p.InstitutionCode,p.Bank,p.BankName,p.BankBranch,c.NotificationEndPoint,p.ReceiptNumber,p.Amount from PaymentTransactions p, ConsultantInfo c where P.InstitutionCode='2039LA003626928' and isPush IS NULL and c.ConsultantCode = 'SOFTA' ";

                SqlCommand cmd = new SqlCommand(query, cn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);

                for (int i = 0; dt.Rows.Count > i; i++)
                {
                    SqlConnection cn1 = new SqlConnection(ConfigurationManager.ConnectionStrings["XpressDb"].ConnectionString);
                    cn1.Open();
                    try
                    {
                        double original_amount = Convert.ToDouble(ConfigurationManager.AppSettings["mscCharge"]) * Convert.ToDouble(dt.Rows[i].ItemArray.GetValue(2).ToString().Replace("'", "''"));
                        string amount = (Math.Round(original_amount / 1000) * 1000).ToString() + ".00";


                        string query1 = "INSERT INTO NotificationTable(MerchantCode,TransactionId,Amount,MaskedPan,TerminalId,DepositorName,CustomerName,RevenueCode,TransactionDate,CurrencyCode,ReceiptNumber,InstitutionCode,BankCode,BankName,BranchCode,NotificationEndpoint,SubItemTransactionId,TotalAmount,ConsultanCode,Status) VALUES ('"
                            + dt.Rows[i].ItemArray.GetValue(0).ToString().Replace("'", "''") + "','"
                            + dt.Rows[i].ItemArray.GetValue(1).ToString().Replace("'", "''") + "','"
                            + amount + "','"
                            + dt.Rows[i].ItemArray.GetValue(3).ToString().Replace("'", "''") + "','"
                            + dt.Rows[i].ItemArray.GetValue(4).ToString().Replace("'", "''") + "','"
                            + dt.Rows[i].ItemArray.GetValue(5).ToString().Replace("'", "''") + "','"
                            + dt.Rows[i].ItemArray.GetValue(6).ToString().Replace("'", "''") + "','"
                            + dt.Rows[i].ItemArray.GetValue(7).ToString().Replace("'", "''") + "','"
                            + dt.Rows[i].ItemArray.GetValue(8).ToString().Replace("'", "''") + "','"
                            + dt.Rows[i].ItemArray.GetValue(9).ToString().Replace("'", "''") + "','"
                            + dt.Rows[i].ItemArray.GetValue(10).ToString().Replace("'", "''") + "','"
                            + dt.Rows[i].ItemArray.GetValue(11).ToString().Replace("'", "''") + "','"
                            + dt.Rows[i].ItemArray.GetValue(12).ToString().Replace("'", "''") + "','"
                            + dt.Rows[i].ItemArray.GetValue(13).ToString().Replace("'", "''") + "','"
                            + dt.Rows[i].ItemArray.GetValue(14).ToString().Replace("'", "''") + "','"
                          + dt.Rows[i].ItemArray.GetValue(15).ToString().Replace("'", "''") + "','"
                            + dt.Rows[i].ItemArray.GetValue(16).ToString().Replace("'", "''") + "','"
                            + amount + "','SOFTA','0'); update paymentTransactions set ispush='1' where transId='" + dt.Rows[i].ItemArray.GetValue(1).ToString().Replace("'", "''") + "'";
                        SqlCommand cmd1 = new SqlCommand(query1, cn1);
                        cmd1.ExecuteNonQuery();
                        var resp = "SUCCESSFULLY PROFILED FOR NOTIFICATION TO SOFT ALLIANCE";
                        logger.Info("\n\n Transaction with Transaction ID " + dt.Rows[i].ItemArray.GetValue(1).ToString() + " ==> " + resp + " \n");
                    }
                    catch (Exception ex)
                    {
                        var err = ex.Message;
                        logger.Error("\n\n Transaction with transaction ID " + dt.Rows[i].ItemArray.GetValue(1).ToString() + " ==> " + ex.Message + " and was unable to profile for Notification to Soft Alliance\n");
                    }
                    finally
                    {
                        cn1.Close(); cn1.Dispose();
                    }

                }

            }
            catch (Exception ex)
            {

            }
            finally
            {
                cn.Close(); cn.Dispose();
            }
        }

    }
}
